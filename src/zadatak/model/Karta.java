package zadatak.model;

public class Karta {

	protected int id;
	protected String name;
	protected String lastName;
	protected Zicara zicara;
	protected int uses;
	protected double average;
	
	
	public Karta(String name, String lastName, Zicara zicara) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.zicara = zicara;
	}
	public Karta(int id, Zicara zicara, double average) {
		super();
		this.id = id;
		this.zicara = zicara;
		this.average = average;
	}
	public Karta(int id, String name, String lastName, Zicara zicara, int uses) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.zicara = zicara;
		this.uses = uses;
	}
	
	
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Zicara getZicara() {
		return zicara;
	}
	public void setZicara(Zicara zicara) {
		this.zicara = zicara;
	}
	public int getUses() {
		return uses;
	}
	public void setUses(int uses) {
		this.uses = uses;
	}
	@Override
	public String toString() {
		return "Karta [id=" + id + ", name=" + name + ", lastName=" + lastName + ", zicara=" + zicara + ", uses=" + uses
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + uses;
		result = prime * result + ((zicara == null) ? 0 : zicara.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Karta other = (Karta) obj;
		if (id != other.id)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (uses != other.uses)
			return false;
		if (zicara == null) {
			if (other.zicara != null)
				return false;
		} else if (!zicara.equals(other.zicara))
			return false;
		return true;
	}
	
	
	
}
