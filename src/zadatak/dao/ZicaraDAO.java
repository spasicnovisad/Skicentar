package zadatak.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import zadatak.model.Zicara;

public class ZicaraDAO {
	
	public static Zicara getId(int id) throws Exception {
		
		Zicara zicara = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, zName FROM skicentar.zicare WHERE id = " + id;
			
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			while (rset.next()) {
				int i = 1;
				int zId = rset.getInt(i++);
				String zNae = rset.getString(i++);
				
				zicara = new Zicara(zId, zNae);
				return zicara;
				
			}
		} finally {
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			try {rset.close();} catch (Exception e) {e.printStackTrace();}
		}
		return zicara;
	}
	
	
	public static ArrayList<Zicara> getAll() throws Exception {
		
		ArrayList<Zicara> zicare = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, zName FROM skicentar.zicare;";
			
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			while (rset.next()) {
				int i = 1;
				int id = rset.getInt(i++);
				String zNae = rset.getString(i++);
				
				Zicara zicara = new Zicara(id, zNae);
				zicare.add(zicara);
				
			}
		} finally {
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			try {rset.close();} catch (Exception e) {e.printStackTrace();}
		}
		return zicare;
	}
	
	

}
