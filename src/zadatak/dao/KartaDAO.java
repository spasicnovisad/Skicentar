package zadatak.dao;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import zadatak.model.Karta;
import zadatak.model.Zicara;
import zadatak.util.PomocnaKlasa;

public class KartaDAO {

	public static ArrayList<Karta> getAll() throws Exception {
		ArrayList<Karta> karte = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, guestName, guestLastName, zId, uses FROM skicentar.karte";
			
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			while (rset.next()) {
				int i = 1;
				int id = rset.getInt(i++);
				String guestName = rset.getString(i++);
				String guestLastName = rset.getString(i++);
				int zId = rset.getInt(i++);
				int uses = rset.getInt(i++);
			
				
				Zicara zicara = ZicaraDAO.getId(zId);
				Karta karta = new Karta(id, guestName, guestLastName, zicara, uses);
				
				karte.add(karta);
			}
		} finally {
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			try {rset.close();} catch (Exception e) {e.printStackTrace();}
		}
		return karte;
	}
	
	public static Karta getId(int id) throws Exception {
		
		Karta karta = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, guestName, guestLastName, zId, uses FROM skicentar.karte WHERE id = " + id;
			
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			while (rset.next()) {
				int i = 1;
				int idSql = rset.getInt(i++);
				String guestName = rset.getString(i++);
				String guestLastName = rset.getString(i++);
				int zId = rset.getInt(i++);
				int uses = rset.getInt(i++);
			
				
				Zicara zicara = ZicaraDAO.getId(zId);
				karta = new Karta(idSql, guestName, guestLastName, zicara, uses);

				return karta;
			}
		} finally {
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			try {rset.close();} catch (Exception e) {e.printStackTrace();}
		}
		return karta;
	}
	
	public static boolean update(Karta karta) throws Exception {
		
		PreparedStatement stmt = null;
		
		try {
			String sql = "UPDATE skicentar.karte SET uses = ? WHERE id = " + karta.getId();
			
			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			stmt.setInt(1, karta.getUses());
			
			return stmt.executeUpdate() == 1;
		} finally {
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
		}	
	}
	
	public static ArrayList<Karta> average() throws Exception {
		
		ArrayList<Karta> karte = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, zId, AVG(uses) AS average FROM skicentar.karte GROUP BY zId";
		
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			while (rset.next()) {
				int i = 1;
				int id = rset.getInt(i++);
				int zId = rset.getInt(i++);
				double average = rset.getDouble(i++);
				
				Zicara zicara = ZicaraDAO.getId(zId);
				
				Karta karta = new Karta(id, zicara, average);
				karte.add(karta);
			}
		} finally {
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			try {rset.close();} catch (Exception e) {e.printStackTrace();}
		}
		return karte;
	}
	
//	public static void save(String path) {		
//		
//		try {
//			List<Karta> karte = new ArrayList<>();
//			Karta karta1 = new Karta("A", "T", ZicaraDAO.getId(1));
//			Karta karta2 = new Karta("T", "T", ZicaraDAO.getId(1));
//			Karta karta3 = new Karta("E", "T", ZicaraDAO.getId(1));
//			Karta karta4 = new Karta("B", "Y", ZicaraDAO.getId(2));
//			karte.add(karta1);
//			karte.add(karta2);
//			karte.add(karta3);
//			karte.add(karta4);
//			
//			FileWriter writer = new FileWriter(path);
//			for(Karta k : karte) {
//				writer.write(k.getName()+";"+k.getLastName()+";"+k.getZicara().getId()+"\n");
//			}
//			writer.close();
//			
//			
//		} catch (Exception e) { 
//			e.printStackTrace();
//		}
//	}
	
	public static void load(String path) throws Exception {
		
		PreparedStatement stmt = null;
		
		try {
		
			List<String> karte = Files.readAllLines(Paths.get(path));
			
			for(String s : karte) {
				String[] line = s.split(";");
				
				String name = line[0];
				String last = line[1];
				int zId = Integer.parseInt(line[2]);
				
				String sql = "INSERT INTO skicentar.karte (guestName, guestLastName, zId, uses) VALUES "
						+ "(?, ?, ?, 10)";
				
				stmt = ConnectionManager.getConnection().prepareStatement(sql);
				int i = 1;
				stmt.setString(i++, name);
				stmt.setString(i++, last);
				stmt.setInt(i++, zId);
				
				stmt.executeUpdate();
			}
		
		
		} finally { 
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
		}
	}
}
