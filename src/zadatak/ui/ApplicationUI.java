package zadatak.ui;

import zadatak.dao.ConnectionManager;
import zadatak.dao.KartaDAO;
import zadatak.util.PomocnaKlasa;

public class ApplicationUI {

	private static void ispisiMenu() {
		System.out.println();
		System.out.println("Meni:");
		System.out.println("=====================================");
		System.out.println("\t1 -  Prikazi sve zicare");
		System.out.println("\t2 -  Prikazi sve karte ");
		System.out.println("\t3 -  Evidencija ulaska");
		System.out.println("\t4 -  ");
		System.out.println("\t5 -   ");
		System.out.println("\t6 -   ");
		System.out.println("\tx - IZLAZ IZ PROGRAMA");
	}
	
	public static void main(String[] args) {
		try {
			ConnectionManager.open();
		} catch (Exception ex) {
			System.out.println("Neuspesna konekcija na bazu!");
	
			ex.printStackTrace();
			return;
		}
	
//		KartaDAO.save("karte.txt");
		
		String odluka = "";
		while (!odluka.equals("x")) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajTekst();
			switch (odluka) {				
				case "1":
					ZicaraUI.showAll();
					break;
				case "2":
					KartaUI.showTickets();
					break;
				case "3":
					KartaUI.evidencija();
					break;
				case "4":
					KartaUI.average();
					break;
				case "5":
					try {
						KartaDAO.load("karte.txt");
					} catch (Exception e) {
						e.printStackTrace();
					}
					KartaUI.showTickets();
					break;
				case "x":
					System.out.println("Izlaz");
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;
			}
		}
	
		try {
			ConnectionManager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
