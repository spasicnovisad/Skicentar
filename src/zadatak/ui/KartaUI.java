package zadatak.ui;

import java.util.ArrayList;

import zadatak.dao.KartaDAO;
import zadatak.model.Karta;
import zadatak.util.PomocnaKlasa;

public class KartaUI {

	public static void showTickets() {
		
		ArrayList<Karta> karte = null;
		
		try {
			karte = KartaDAO.getAll();
			
			for(Karta k : karte) { 
				System.out.printf("%-10d %-15s %-15s %-15s %-10d ", 
						k.getId(),
						k.getName(),
						k.getLastName(),
						k.getZicara().getName(),
						k.getUses());System.out.println();
			}
		} catch (Exception e ) {
			e.printStackTrace();
		}
	}
	
	public static void evidencija() {
		
		Karta karta = null;
		try {
			System.out.println("Unesi ID karte ");
			karta = KartaDAO.getId(PomocnaKlasa.ocitajCeoBroj());
			
			if(karta == null) {
				System.out.println("Nepostoji karta sa zadatim IDem");
				return;
			} else if (karta.getUses() == 0) {
				System.out.println("Istrosili ste karticu, molim vas dopunite");
				return;
			} else if (karta.getUses() > 0) {
				karta.setUses(karta.getUses() - 1);
					if(karta.getUses() == 0) {
						System.out.println("Nemate vise kredita na kartici");
						KartaDAO.update(karta);
						return;
					} else {
						System.out.println("Imate jos " + karta.getUses() + " koriscenja.");
						KartaDAO.update(karta);
						return;
					}
			}
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	
	public static void average() {
		
		ArrayList<Karta> karte = null;
		
		try {
			karte = KartaDAO.average();
			
			for(Karta k : karte) {
				System.out.printf("%-10s %-10.2f",
						k.getZicara().getName(),
						k.getAverage());System.out.println();
			}
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	
}
